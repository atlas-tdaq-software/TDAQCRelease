"""
When importing a (top level) module, check the modules.json file
that caches the relative locations of modules -> file paths in
the LCG release.
"""
modules = {}
dists   = {}

lcg_root = None
tag = None

class lcg_hook:

   @staticmethod
   def find_spec(fullname, path, target):
      """
      Find modules in LCG area by looking up a generated map from modules.json
      We handle only top level imports and leave the to the standard handlers.
      """
      if path != None: return None

      import sys
      import os

      if fullname in modules:
         for m in modules[fullname]:
            fullpath = '/'.join([lcg_root, m, f'{tag}/lib/python{sys.version_info.major}.{sys.version_info.minor}/site-packages'])
            if not fullpath in sys.path:
               sys.path.append(fullpath)
      # Let the standard path finder do the rest
      return None

   @staticmethod
   def find_distributions(context):
       import importlib
       if context.name in dists:
          for pkg in dists[context.name]:
              for path in modules[pkg]:
                  fullpath = '/'.join([lcg_root, path, f'{tag}/lib/python{sys.version_info.major}.{sys.version_info.minor}/site-packages'])
                  if not fullpath in sys.path:
                      sys.path.append(fullpath)
       return []

try:
   import json
   import os
   import sys

   lcg_root = '/'.join(sys.base_prefix.split('/')[0:-3])
   tag      = sys.base_prefix.split('/')[-1]

   with open(os.environ.get("LCG_PYTHON_MODULES", os.path.dirname(__file__) + '/modules.json')) as f:
      info = json.load(f)
      modules = info["modules"]
      dists   = info["dists"]
      sys.meta_path.insert(0, lcg_hook)

except Exception as ex:

   # Marco's original code in the LCG sitecustomize.py
   import sys
   from os.path import basename, dirname, join, isdir, exists
   from os import listdir

   lcg_root = dirname(dirname(dirname(sys.base_prefix)))
   platform = basename(sys.base_prefix)
   suffix = join(platform, 'lib', 'python%d.%d' % sys.version_info[:2],
                 'site-packages')

   if exists(join(lcg_root, 'LCG_externals_%s.txt' % platform)):
      sys.path.extend(
         join(pkg, vers, suffix) for pkg in [
            join(lcg_root, pkg) for pkg in sorted(listdir(lcg_root))
            if isdir(join(lcg_root, pkg))
         ] for vers in listdir(pkg) if isdir(join(pkg, vers, suffix)))
